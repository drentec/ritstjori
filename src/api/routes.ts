import express from "express";
import { Post } from "../lib/model/Post";

export function api() {
    const app = express();

    console.log("Start api");

    app.get("/post", function (_req, res) {
        
        const posts: Array<Post> = [];

        for (let i = 0; i < 10; i++) {
            posts.push(new Post(`Post ${i}`, `hello world ${i}`));
        }
    
        res.json(posts);
    });

    return app;
}