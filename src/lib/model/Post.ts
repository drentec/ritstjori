import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Post {

    @PrimaryGeneratedColumn()
    id?: number;

    @Column()
    private title: string;

    @Column()
    private text?: string;

    constructor(title: string, text?: string) {
        this.title = title;
        this.text = text;
    }
}