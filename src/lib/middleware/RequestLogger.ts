import { NextFunction, Request, Response } from "express";
import loggerFactory from "../logger";
import Middleware from "./Middleware";

const logger = loggerFactory("RequestLogger");

export const requestLoggerMiddleware: Middleware = function(req: Request, res: Response, next: NextFunction): void {
    logger.info(`${req.method}: ${req.path}`);
    
    return next();
};