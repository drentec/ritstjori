import express from "express";
import { api } from "../api/routes";
import { requestLoggerMiddleware } from "./middleware/RequestLogger";
import "reflect-metadata";  // import for typeorm

const app = express();

// add middleware
app.use(requestLoggerMiddleware);

// add submodules
app.use("/api", api());

// global routes
app.get("/health-check", (_req, res) => res.sendStatus(200));

export default app;