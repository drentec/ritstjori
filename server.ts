import app from "./src/lib/app";

app.get("/", function(req, res) {
    res.send("Headless CMS");
});

app.listen(3000, function() {
    console.log("Server is running...");
    console.log("Server is listening on port 3000");
});